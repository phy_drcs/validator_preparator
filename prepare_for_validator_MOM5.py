# This is just an example how to use the validator preparator
# If you want to use it yourself, you have to adapt the paths and probably
# the variables you want to extract. 

# import necessary python modules
import glob

# import validator preparator modules:
# base classes
import validator_preparator

station_names = {
    "AnholtE"           : "anholte",
    "ArkonaBY2"         : "by2",
    "BornholmDeepBY5"   : "by5",
    "BothnianBayF9"     : "f9",
    "BothnianSeaSR5"    : "sr5",
    "BothnianSeaUS3"    : "us3",
    "FehmarnBelt"       : "fehnmarn",
    "GdanskDeep"        : "gdansk",
    "GotlandDeepBY15"   : "by15",
    "GreatBelt"         : "greatbelt",
    "GulfFinlandLL7"    : "ll7",
    "GulfofFinlandF1"   : "f1",
    "GulfofRiga"        : "gulfofriga",
    "LandskronaW"       : "landskron",
    "LandsortDeepBY31"  : "by31",
    "SEGotlandBasin"    : "segotlandbasin"
    }

variable_names     = {
    "salt" : "MODEL_SALIN",
    "temp" : "MODEL_TEMP"
    }

depth_pattern = "st_ocean_*"

# prepare parameters for the preparator
parameters = validator_preparator.ValidatorParameters()

# build up list of files (use absolute paths, with read permissions!)
data_pattern = "tests/data/rregion_*.nc*"
parameters.paths_to_data = glob.glob(data_pattern)
# where should the output be stored (you need write permissions there)
parameters.path_to_output = "tests/output"
# choose the variables you want to consider (default is all available)
parameters.variables = ["temp", "salt",]
# choose stations you want to consider (default is all available)
parameters.stations = ["ArkonaBY2"]

# return value has to be one of the keys in self.station_names
def extract_station_name(file_name):
    return file_name.split("rregion_")[1].split(".nc.")[0].split("_")[0]

# create an instance of a concrete vaildator preparator (in this case MOM5)
# instantiate it with the parameters object
preparator = validator_preparator.ValidatorPreparator(parameters=parameters, 
                                                      variable_names=variable_names, 
                                                      station_names=station_names, 
                                                      depth_pattern=depth_pattern,
                                                      extract_station_name_function=extract_station_name)
# do the preparation
preparator.prepare_for_validation()

# have a look into output path, 
# copy files to a place where the validator can handle it (/silod5)
# be sure that all have read permissions for this location