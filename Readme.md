# Validator preparator

[![Build Status](http://drone.io-warnemuende.de/api/badges/phy_drcs/validator_preparator/status.svg)](http://drone.io-warnemuende.de/phy_drcs/validator_preparator)

The Validator is a web-based interactive tool for validation of ocean models at oceanographic stations. 

## 0. How to access

This documentation focuses on the usage within the IOW intranet, which gives access to the station data of the IOW_DATABASE. The validator can be accessed at 

[http://10.10.10.20/radtke/validator/](http://10.10.10.20/radtke/validator/)

Note that the IP is provided here, since some persons have problems with their DNS server when using a VPN.

Once opened you will see pre-configured model data. To add new model data, tik the box of **other models** on the left column and upload your model list. You will also find the option to download a sample model list that helps with the sytnax.

## 1. How to use

There is no common model output. Therefore, an example on the usage of the existing validator preparator can be found in `prepare_for_validator_MOM5.py`.

This an example suitable for validation of the MOM5 model. 
Please read the comments therein.

Note that if you still need a more specialized method to extract the station name from the file names
you can derive a new class from the existing one like e.g.

``` python
#...
import validator_preparator_MOM5

# initialize the concrete class
class ValidatorPreparatorMOM5Special(validator_preparator_MOM5.ValidatorPreparatorMOM5):
    def __init__(self, parameters):
        # call base constructor, pass the parameters
        validator_preparator_MOM5.ValidatorPreparatorMOM5.__init__(self, parameters)
        
    # overrides base-class method
    # return value has to be one of the keys in self.station_names
    def extract_station_name(self, file_name):
        return "do a very special thing here"
		
# prepare parameters for the preparator
parameters = validator_preparator.ValidatorParameters()
#...

# create an instance of a concrete vaildator preparator (in this case your special MOM5)
# instantiate it with the parameters object
preparator = ValidatorPreparatorMOM5Special(parameters)
#...
```

## 2. How to develop

If you want to extend the validator preparator for another model, you have to create a new module, e.g. `validator_preparator_XXX.py`.
It is intended that new modules define a class that inherits from the base class `ValidatorPreparator` in `validator_preparator.py`.
For the implementation details please have a look at the comments therein.

In the above example the module could look like:

``` python
# import the base class
import validator_preparator

# define a dictionary that relates XXX variable names to validator variable names 
# the keys (left) are the model variables, the corresponding values are the validator variables
variable_names     = {
    "XXX_temp" : "MODEL_TEMP"
    }

# define a dictionary that relates XXX station names to validator station names
# the keys (left) are the model stations, the corresponding values are the validator stations
station_names = {
    "XXX_anholte"           : "anholte"
    }

# define how the depth pattern for the model looks like
depth_pattern = "XXX_depth*"

# initialize the concrete class
class ValidatorPreparatorXXX(validator_preparator.ValidatorPreparator):
    def __init__(self, parameters):
        # call base constructor, pass the concrete dictionaries defined above
        validator_preparator.ValidatorPreparator.__init__(self, parameters, variable_names, station_names, depth_pattern)
        
    # overrides base-class method
    # return value has to be one of the keys in self.station_names
    def extract_station_name(self, file_name):
        return "whatever has to be done here"
```

Naturally, the strings starting with `XXX_` should be replaced by the actual names of your model
and the dictionaries should be completed for your purposes.

Importantly, the method `extract_station_name` has to be implemented such 
that it returns one of the `station_name` keys.

The rest should be done by the base class and there is no need for further implementation.
It would be nice if a usage example for the new module is added to the repository.