#import python modules
from netCDF4 import Dataset
import os, re
import glob
import xarray as xr


# class for parameters to initialize a preparator instance
class ValidatorParameters:
    def __init__(self):
        # list of absolute paths to data files
        self.paths_to_data = []
        # absolute path for storing the output
        self.path_to_output = ""
        # variables to consider, if left empty all available variables are considered (call _check_variables)
        self.variables = []
        # stations to consider, if left empty all available stations are considered (call _check_stations)
        self.stations = []
        self.use_three_dimensional_data = False

class ValidatorPreparator:

    # constructor
    def __init__(self, parameters, variable_names, station_names, depth_pattern, time_name="time", extract_station_name_function = None):
        # store parameters
        self.parameters = parameters
        
        # store variables and stations
        self.variables = self.parameters.variables
        self.stations = self.parameters.stations
        
        # initialize the names of variables, stations and depth pattern
        # concrete definition has to be done in concrete-class module
        self.variable_names = variable_names
        self.station_names = station_names
        self.depth_pattern = depth_pattern
        self.time_name = time_name
        self.extract_station_name_function = extract_station_name_function
        
        self.use_three_dimensional_data = False

        # auxiliary variable to gather files corresponding to single station
        self.station_files = {}
        
    def reconfigure(self, station_names, variable_names="", depth_pattern="",  time_name="", extract_station_name_function = None):
        if station_names != "":
            self.station_names = station_names
            
        if variable_names != "":
            self.variable_names = variable_names
            
        if depth_pattern != "":
            self.depth_pattern = depth_pattern
            
        if time_name != "":
            self.time_name = time_name
            
        if extract_station_name_function is not None:
            self.extract_station_name_function = extract_station_name_function
        
    def _check_variables(self):
        """check if given variables are available"""
        for var in self.variables:
            if var not in self.variable_names.keys():
                raise ValueError(var + " is not in " + str(self.variable_names.keys()))
                
        # if given list is empty (default) all variables are considered
        if self.variables == []:
            self.variables = list(self.variable_names.keys())
                
    def _check_stations(self):
        """check if given stations are available"""
        for station in self.stations:
            if station not in self.station_names.keys():
                raise ValueError(station + " is not in " + str(self.station_names.keys()))
        
        # if given list is empty (default) all stations are considered
        if self.stations == []:
            self.stations = list(self.station_names.keys())


    def extract_station_name(self, file_name):
        """ Method to extract station name from file name
            
        Very model-specific and is thus to be implemented in concrete class.
        The return value has to be one of the keys in self.station_names
        """
        if self.extract_station_name_function is None:
            raise NotImplementedError("Method " + self.extract_station_name.__name__ +
                                  " has to be implemented by the concrete class!")
        else:
            return self.extract_station_name_function(file_name)

    def get_stations(self):
        if self.use_three_dimensional_data == False:
            print("Get stations from data files...")
            # go through list of data files
            for file in self.parameters.paths_to_data:
                # obtain the station name from the file name
                station_name = self.extract_station_name(file)
                
                # build up a list of file names corresponding to a single station
                if(station_name in self.stations):
                    if(station_name not in self.station_files.keys()):
                        print(" Found new station " + station_name)
                        self.station_files[station_name]=[]
                    self.station_files[station_name].append(file)
                else:
                    # Note that some station may be skipped
                    print("Warning: Station " + station_name + 
                        " not found in " + str(self.station_names))
                    continue 
            print("...done.")
        else:
            self.station_files = self.parameters.paths_to_data
            
    def process_station_data(self):
        print("Process station data...")
        # create output directory, if not existing
        if not os.path.isdir(self.parameters.path_to_output):
            os.mkdir(self.parameters.path_to_output)
            print("Created " + self.parameters.path_to_output)
            
        # process data for all stations
        for station in self.stations:
            print(" At station " + station + "...")
            # list for temporary files that are removed afterwards
            output_files = []
            #TODO ADD HERE LOOP FOR 3D DATA
            # go through all files corresponding to the current station
            for i, file in enumerate(self.station_files[station]):
                try:
                    # get the data
                    ds = xr.open_dataset(file, engine="netcdf4")
                    
                    # transform depth pattern into regular expression
                    depth_pattern = re.compile(self.depth_pattern)
                    
                    # get all variable names that match the depth pattern
                    depth_name = [x for x in ds.dims if depth_pattern.match(x)][0]
            
                    # rename all depth pattern names uniformly to "z" (required from the validator)
                    ds = ds.rename({depth_name:"z"})
                    
                    # reduce data to considered variables
                    ds = ds[self.variables].squeeze()
                    
                    # rename model variables to validator variables
                    for var in self.variables:
                        ds = ds.rename({var:self.variable_names[var]})
                       
                    # store the result into temporary file
                    output_files.append(self.parameters.path_to_output + "/" + station + "_" +str(i) + ".nc")
                    ds.to_netcdf(output_files[-1])
                    ds.close()
                    
                except Exception as e:
                    print("Unable to extract data from " + file + ". Caught exception ",e)
                    continue    
    
            try:
                # open all temporary files corresponding to one station
                ds = self.read_netcdfs(self.parameters.path_to_output + 
                                       "/" + station +"*.nc*",
                                       dim = self.time_name,
                                       transform_func = None) 
 
		# merge the data into one station-named file
                ds.to_netcdf(self.parameters.path_to_output + 
                             "/" + self.station_names[station] + ".nc")
                ds.close()
                    
            except Exception as e:
                print("Unable to station " + station + ". Caught exception ", e)
                
            # remove temporary files
            for file in output_files:
                os.remove(file)
                
            print(" ...done.")
            
        print("...done.")
                
    # this is the main routine that should be called from outside           
    def prepare_for_validation(self):
        # check if the given stations and variables are available
        self._check_variables()
        if self.use_three_dimensional_data == False:
            self._check_stations()
            self.get_stations()
        
        self.process_station_data()
                    
            
    def print_self(self):
        # can be used for debugging
        print(vars(self.parameters))
        print(vars(self))

    def read_netcdfs(self, files, dim, transform_func, transform_calendar = None, cftime = True):
        """Reads multiples netcdfs files. Should be used when open_mfdatasets is to slow."""
        def process_one_path(path):
            if transform_calendar is not None:
                calendar = False
            else:
                calendar = True
            with xr.open_dataset(path, decode_times = calendar, use_cftime = cftime) as ds:
                if transform_calendar is not None:
                    ds[dim].attrs['calendar'] = transform_calendar
                    ds = xr.decode_cf(ds, use_cftime = cftime)
                if transform_func is not None:
                    ds = transform_func(ds)
                ds.load()
                return ds
        paths = sorted(glob.glob(files))
        datasets = [process_one_path(p) for p in paths]
        combined = xr.concat(datasets, dim)
        return combined   
    

